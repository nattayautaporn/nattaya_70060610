using System.IO;
using System.Threading.Tasks;
using api.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;

namespace api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CarBookingsController : ControllerBase
    {
        private readonly ApplicationContext _context;

        public CarBookingsController(ApplicationContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult> GetCarBookings()
        {
            var result = await _context.CarBooking.Select(x => new
            {
                x.Id,
                ReserveDate = x.ReserveDate.ToString("yyyy-MM-dd"),
                ReserveTime = x.ReserveTime.ToString("HH:mm"),
                x.Remark,
                x.LicensePlate
            }).ToListAsync();

            return Ok(result);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<ActionResult> GetCarBooking(int id)
        {
            var result = await _context.CarBooking.FirstOrDefaultAsync(c => c.Id == id);
            return Ok(result);
        }

        [HttpPost]
        public async Task<ActionResult<bool>> AddCarBooking(CarBooking carBooking)
        {
            var dataDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day).AddDays(3);
            if (carBooking.ReserveDate < dataDate)
            {
                return false;
            }
            _context.CarBooking.Add(carBooking);
            await _context.SaveChangesAsync();
            return Ok(true);
        }

        [HttpPut]
        public async Task<ActionResult> UpdateCarBooking(CarBooking carBooking)
        {
            _context.CarBooking.Update(carBooking);
            await _context.SaveChangesAsync();
            return Ok();
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<ActionResult> DeleteCarBooking(int id)
        {
            var carBooking = await _context.CarBooking.FirstOrDefaultAsync(c => c.Id == id);
            if (carBooking != null)
            {
                _context.CarBooking.Remove(carBooking);
                await _context.SaveChangesAsync();
            }

            return Ok();
        }

        [HttpGet("export")]
        public async Task<ActionResult> ExportCarBooking()
        {
            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            var stream = new MemoryStream();
            using var package = new ExcelPackage(stream);
            var workSheet = package.Workbook.Worksheets.Add("Sheet1");

            workSheet.Cells[1, 1].Value = "Id";
            workSheet.Cells[1, 2].Value = "วันที่ต้องการจอง";
            workSheet.Cells[1, 3].Value = "เวลาที่ต้องการจอง";
            workSheet.Cells[1, 4].Value = "เหตุผลการจอง";
            workSheet.Cells[1, 5].Value = "ทะเบียนรถ";

            var CarBooking = await _context.CarBooking.ToListAsync();
            var rowIndex = 2;

            foreach (var car in CarBooking)
            {
                workSheet.Cells[rowIndex, 1].Value = car.Id;
                workSheet.Cells[rowIndex, 2].Value = car.ReserveDate.ToString("dd/MM/yyyy");
                workSheet.Cells[rowIndex, 3].Value = car.ReserveTime.ToString("HH:mm");
                workSheet.Cells[rowIndex, 4].Value = car.Remark;
                workSheet.Cells[rowIndex, 5].Value = car.LicensePlate;
                rowIndex++;
            }

            return File(new MemoryStream(package.GetAsByteArray()), "application/vnd.ms-excel", "CarBooking");
        }
    }
}