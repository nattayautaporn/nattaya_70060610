using System.ComponentModel.DataAnnotations;

namespace api.Entities
{
    public class CarBooking
    {
        public int Id { get; set; }
        public DateTime ReserveDate { get; set; }
        public DateTime ReserveTime { get; set; }
        [StringLength(100)]
        public string? Remark { get; set; }
        [StringLength(100)]
        public string? LicensePlate { get; set; }

    }
}