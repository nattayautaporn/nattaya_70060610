using Microsoft.EntityFrameworkCore;

namespace api.Entities
{
    public class ApplicationContext : DbContext
    {
        public DbSet<CarBooking> CarBooking { get; set; } = default!;
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {

        }
    }
}