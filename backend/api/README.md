

***** อย่าลืม Remove Migrations ก่อนนำไปใช้งาน*******

1. สร้าง API project โดยเลือกเป็น web api
dotnet new API
2. รัน 
dotnet run
3. เพิ่ม .net ef package
dotnet add package Microsoft.EntityFrameworkCore.SqlServer 
dotnet add package Microsoft.EntityFrameworkCore.Design 
dotnet add package Microsoft.EntityFrameworkCore.Tools 
dotnet add package Microsoft.AspNetCore.Diagnostics.EntityFrameworkCore 
4. สร้าง model สำหรับ database
   Entities>Student.cs
5. สร้าง context
   Entities>ExampleContext.cs
   จากนั้น inherit DbContext
6. เพิ่ม default connection string ของ Example context ใน startup
   services.AddDbContext<ExampleContext>(options =>
      options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

7. Add migration
dotnet ef migrations add InitialDatabase

8. Remove migration ล่าสุด (ถ้าจำเป็น)
dotnet ef migrations remove

9. update dabase
dotnet ef database update

# สร้าง Repository
1. สร้าง Interface Repository
Repositories>IStedentRepository.cs
2. สร้าง Repository
Repositories>StudentRepository.cs
3. Inject repo ในไฟล์ startup ที่ scope ConfigureServices
services.AddScoped<IStudentRepository, StudentRepository>();

# สร้าง Service
1. สร้าง Interface Service
Services>IStudentService.cs
2. สร้าง Service
Services>StudentService.cs
3. Inject service ในไฟล์ startup ที่ scope ConfigureServices
services.AddScoped<IStudentService, StudentService>();
4. เพิ่ม auto mapper เพื่อ map model ของ entity กับ data transfer object
dotnet add package AutoMapper --version 10.1.1
dotnet add package AutoMapper.Extensions.Microsoft.DependencyInjection
5. สร้าง model mapping profile เพื่อบอกว่า model ไหน ควร map กัน
Models>MappingProfile.cs
CreateMap<Student, StudentDto>().ReverseMap();
6. ที่ startup เพิ่ม automapper
services.AddAutoMapper(typeof(MappingProfile));

# สร้าง Controller
1. สร้าง Controller
Controllers>StudentController.cs
2. Inherit Controller ที่ class StudentController
StudentController: Controller
3. ประกาศ private readonly service ที่จะใช้งานใน class
4. ประกาศ constructor แล้ว inject service ที่จะใช้งาน
5. ประกาศ attribute บนหัว class ว่าเป็น ApiController
6. ประกาศ attribute บนหัว class ว่า Route ของ controller นี้ จะเป็น path อะไร
## สร้าง Method
1. ประกาศ method เป็น public และ return Task<ActionResult>

# Add Package
dotnet add package ชื่อแพ็คเกจ --version เลขเวอร์ชั่น
เช่น
dotnet add package AutoMapper --version 10.1.1


### การ create table 
### dotnet ef migrations add InitialDatabase
### dotnet ef database update