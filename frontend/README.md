# frontend

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


// -------------------------

### persistent ใน template คือ
ไม่สามารถกด dialog ข้างนอกได้ ต้องกระทำการกดที่ปุ่มอะไรสักปุ่มก่อนใน dialog

### v-model ใน template คือ
การประกาศเรียกใช้ให้มันเปิด - ปิด โดยค่าเริ่มต้น จะอยู่ใน data ซึ่งจะสั่งว่า true หรือ false
false ปิด
true เปิด

### formStage คือการ set ค่า ตัวแปรใน table
### ใน methods 
จะมีการเรียกใช้ ตัวแปร ต่างๆที่อยู่ใน data ได้
โดย มี this นำหน้าด้วย

### table 
v-data-table  :headers="headers"
เเละ :items="rowData"
ซึ่ง headers เเละ rowData คือตัวแปร อะไรก็ได้ที่ต้องส่งค่าไปให้ v-data-table ใน template