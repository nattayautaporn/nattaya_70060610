import axios from "axios";

export default class CarBookingService {
  baseUrl = "https://localhost:7137";

  getCarBookings() {
    return axios.get(`${this.baseUrl}/carBookings`);
  }

  getCarBooking(id) {
    return axios.get(`${this.baseUrl}/carBookings/${id}`);
  }

  addCarBooking(carBooking) {
    return axios.post(`${this.baseUrl}/carBookings`, carBooking);
  }

  editCarBooking(carBooking) {
    return axios.put(`${this.baseUrl}/carBookings`, carBooking);
  }

  deleteCarBooking(id) {
    return axios.delete(`${this.baseUrl}/carBookings/${id}`);
  }

  export() {
    return axios.get(`${this.baseUrl}/carBookings/export`, {
      headers: {
        "Content-Type": "application/json",
      },
      responseType: "blob",
    });
  }
}
